#!/bin/sh
## backup_home.sh

## set usage string
usage="usage: $(basename $0) [-h] [[SRC PATH] [DST PATH]]"

## check opts
while getopts ":h" opt; do
    case $opt in
	h)
	    echo $usage
	    echo -e "\t $(basename $0) -h"
	    echo -e "\t $(basename $0) /home/userdir /mnt/backup/backupdir"
	    exit 1
	    ;;
	?)
	    echo $usage
	    exit 1
	    ;;
    esac
done

## test input for [SRC PATH] && [DST PATH]
[ -z $1 ] && echo "ERROR: set [SRC PATH] to backup" && exit 1
[ -z $2 ] && echo "ERROR: set [DST PATH] location" && exit 1
[ ! -d $1 ] && echo "ERROR: invalid [SRC PATH]" && exit 1
[ ! -d $2 ] && echo "ERROR: invalid [DST PATH]" && exit 1

## get vars
src=$1
dst=$2

echo "backing up ${src} to ${dst} ..."

## run rsync
rsync -aR \
      --exclude "${src}/.npm" \
      --exclude "${src}/.npm-packages" \
      --exclude "${src}/.urxvt" \
      --exclude "${src}/.w3m" \
      --exclude "${src}/.local/bin" \
      --exclude "${src}/.local/lib64" \
      --exclude "${src}/.local/share" \
      --exclude "${src}/.sdkman" \
      --exclude "${src}/.serverauth*" \
      --exclude "${src}/.pki" \
      --exclude "${src}/.mplayer" \
      --exclude "${src}/.android" \
      --exclude "${src}/.cache" \
      --exclude "${src}/go" \
      $src \
      $dst 

exit 0
