#!/bin/sh

export XDG_CACHE_HOME="/var/tmp/usercache/${USER}/.cache"
export XDG_RUNTIME_DIR=$XDG_CACHE_HOME/xdgruntime
mkdir -p $XDG_CACHE_HOME
mkdir -p $XDG_RUNTIME_DIR
